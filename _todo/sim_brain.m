function [phi] = sim_brain(S, N, visual_data)
%SIM_BRAIN Simulacia nervovej sustavy organizmu
%
%   funkcia vykona prechod neuronovej siete na zaklade dat zrakoveho
%   systemu a vrati uhol natocenia organizmu v radianoch
%
%   Na vstupe prichadza :
%       S:              struktura obsahujuca nastavenia simulacie
%       N:              neuronova siet (generovana v init_network)
%       visual_data:    data ziskane zmyslovou sustavou
%   Na vystupe uhol natocenia v radianoch
    
    
    phi = 0;
end