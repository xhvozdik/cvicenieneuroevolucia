function [] = Training(C, global_gen, Pop, LOADED_PLOT_DATA)
    % -----------------------------------------------------------------
    % Inicializacia
    % -----------------------------------------------------------------
    
    if ~exist('C', 'var'); C = config; end   
    if ~exist('global_gen', 'var'); global_gen = 0; end
    
    % LOADED_PLOT_DATA predstavuju PLOT_DATA nactane z ulozeneho experimentu
    if ~exist('LOADED_PLOT_DATA', 'var'); LOADED_PLOT_DATA = {}; end
    
    rng('shuffle');
    fprintf('Training started\n');
    
    % inicializacia triedy pre vizualizaciu simulacie
    V = Visualizator(C);
    V.global_gen = global_gen;
    
    % inicializacia populacie
    if ~exist('Pop', 'var')
        Pop = init_pop(C);
    end
    % ...

    % -----------------------------------------------------------------
    % Priebeh PGA
    % -----------------------------------------------------------------

    % doteraz najlepsia hodnota fitness pre potreby ukladania a vizualizacie
    old_best_fit_animate = inf;
    old_best_fit_save = inf;
    
    for gen = 1:C.gener_n
    % ...

        % (parfor)
        for isle = 1:C.islands_n
        % ...

            for env_id = C.experiment_environments
            % ...

                % priebeh simulacie kazdeho clena populacie pre potreby trenovania
                SIM_DATA = Simulation(C, env_id, Pop{isle}, 1);
                % SIM_DATA = {org_n*org_x, org_n*org_y, org_n*collisions, org_n*org_lifetime, goal_coords}

            end

            % spracovanie vsledkov simulacie
            % ...

            
        end
        
        % data pre potreby vizualizacie a uladania (nutne vyplnit)
        % -----------------------------------------------------------------
        % najlepsia hodnota fitness tejto generacie
        best_fit = 0;
        % najlepsi jedinec GA
        best_chromozome = 0;
        % data pre vykreslovanie grafov (ukladaju sa spolu s populaciou)
        PLOT_DATA = {};
        % index najlepsieho ostrova populacie
        best_isle = 1;
        % -----------------------------------------------------------------
        
        
        fprintf('Gen %d (%d):  Finished, best fitness: %d\n', gen, gen + global_gen, best_fit);
        
        % pripadna animacia najlepsieho jedinca
        do_animate = (old_best_fit_animate > best_fit || ~C.animate_only_better) && mod(gen, C.animate_period) == 0;
        if do_animate
            animate_chromozome(V, C, best_chromozome, gen);
            old_best_fit_animate = best_fit;
        end
        
        % pripadne ukladanie vysledkov
        do_save = (old_best_fit_save > best_fit || ~C.save_only_better) && mod(gen, C.save_period) == 0;
        if do_save
            save_experiment(C, gen + global_gen, Pop, PLOT_DATA, best_isle);
            old_best_fit_save = best_fit;
        end
        
        % nahradenie starej populacie novou na konci generacie
        % ...
    end
end

% -------------------------------------------------------------------------
% Praca s populaciou (inicializacia, vyber, mutacie...)
% -------------------------------------------------------------------------

% inicializacia populacii organizmov
function [Pop] = init_pop(C)
    Pop = cell(1, C.islands_n);
    for i = 1:C.islands_n
        Pop{i} = zeros(C.pop_n, C.genes_n);
    end
end

% ...