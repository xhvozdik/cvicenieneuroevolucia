% KONFIGURACNY SUBOR APLIKACIE

function CONF = config()
    % ---------------------------------------------------------------------
    % hlavne (casto pouzivane) nastavenia aplikacie
    % ---------------------------------------------------------------------
    
    % nazov experimentu (podla neho sa vytvori adresar a zaklad nazvu ulozenych pozicii)
    experiment_name = 'Experment_1';
    
    % prostredia pouzite pri experimentoch
    % mozne hodnoty 1-15
    experiment_environments = [1, 2, 3]; 
    
    % ------------------------- nutne nastavit ----------------------------
    % velkost populacie GA
    pop_n = 5;
    % pocet generacii GA
    gener_n = 5;
    % pocet ostrovov PGA
    islands_n = 1;
    % pocet genov GA (potrebne vypocitat podla rozmerov UNS)
    genes_n = 5;
    % ---------------------------------------------------------------------

    % pocet krokov simulacie
    sim_time = 2500;  

    % kodovanie elementov prostredia v mape prostredia
    %              (prazdny priestor, bariera, potrava/ciel)
    target_codes = [1,                5,       0];
    
    % ---------------------------------------------------------------------
    % nastavenia vizualizacie
    % ---------------------------------------------------------------------
    
    % perioda (v generaciach) animacie najlepsieho jedinca v priebehu treningu 
    animate_period = 5;
    % vykonanie animacie len v pripadem, ze minuly vysledok je lepsi ako 
    % pri predchadzajucej animacii
    animate_only_better = 1;
    % zobrazenie zaciatocnej pozicie organizmu v animacie
    animate_display_starting_position = 1;
    % pocet simulacnych krokov potrebnych pre jeden krok animacie
    % (ovplyvnuje rychlost animacie)
    aniate_refresh_period = 3;
    % zobrazovanie trajektorie organizmu pri animacii
    animate_movement_lines = 1;
    % zobrazenie legendy pri animacii
    animate_legend = 1;

    % ---------------------------------------------------------------------
    % nastavenia organizmu
    % ---------------------------------------------------------------------
    
    % velkost organizmu (s)
    organism_size = 1;
    % velost kroku organizmu (v_s)
    organism_step_size = 1;
    % rozsah dynamickeho kroku organizmu (v_min, v_max)
    organism_step_size_range = [0, 2];
    
    % ---------------------------------------------------------------------
    % nastavenia prostredia
    % ---------------------------------------------------------------------
    
    % velkost prostredia
    base_map_size = 200;
    % hrubka hranice prostredia
    border_width = 1;
    % nahodne zaciatocne pozicie organizmov 
    random_start_position = 0;
    % velkost ciela (hodnota musi byt neparne cislo)
    goal_mode_goal_size = 3;

    % ---------------------------------------------------------------------
    % nastavenia systemu ukladania a nacitavania 
    % ---------------------------------------------------------------------

    % nazov adresara pre ukladanie stavu experimentu
    save_dir = '_saves';
    % nazov adresara pre ukladanie animacii experimentu
    save_animations_dir = '_animations';
    
    % prioda ukladania stavu trenovania experimentu
    save_period = 1;
    % ukladat vysledok len ak uspesnejsi ako v predchadzajucej periode
    save_only_better = 1;
    % ukladanie vytvorenych animacii
    save_animations = 1;
    % v pripade novych nasteveni pri spusteni aplikacie sa ponecha povodny nazov experiemntu
    save_old_experiment_name_with_new_settings = 1;

    % ---------------------------------------------------------------------
    % nastavenia neuronovej siete
    % ---------------------------------------------------------------------
    
    % sposob videnia (1 - lasery, 2 - okolie)
    vision_mode = 1;
    % pocet lucov videnia - musi byt neparne
    rays_n = 5; 
    % uhol vzajomneho natocenia lucov (alpha)
    ray_degree = 15;
    % velkost kroku pri prechode luca prostredim (k)
    ray_step_size = 1;
    % maximalne okolie videnia v 2. mode videnia (r)
    max_sight = 3;
    % rozsah uhla natocenia v radianoch (phi)
    output_range = pi;
    
    % ---------------------------------------------------------------------
    % nastavenia animacie
    % ---------------------------------------------------------------------
    
    % farba trajektorie organizmu
    organism_line_color = 'b';
    % styl trajektorie organizmu
    organism_line_style = '--';
    % marker organimzu 
    organism_marker = 'o';
    % farba markera organizmu
    organism_marker_color = 'r';
    
    % marker bariery v prostredi
    barrier_marker = 'x';
    % farba markera bariery
    barrier_marker_color = 'k';
    % marker znazornujuci koliziu
    collision_marker = 's';
    % farba markera kolizie
    collision_marker_color = 'c';
    % marker potravy/ciela v prostredi
    food_marker = 's';
    % farba markera potravy
    food_marker_color = [0, 0.5, 0];
    % marker pociatocnej pozicie
    starting_position_marker = 'd';
    % farba markera pociatocnej pozicie
    starting_position_marker_color = 'b';
    
    % ---------------------------------------------------------------------
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %	navratove hodnoty a dopocitane parametre NEMENIT
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ---------------------------------------------------------------------
    
    % vypocet velkosti vstupnej vrstvy podla modu videnia, rekurencii, ...
    if vision_mode == 1
        first_layer = rays_n;
        if rays_n / 2 == 0
            first_layer = first_layer + 1;
        end
        first_layer = first_layer*2;
    else
        first_layer = (2*max_sight+1)^2 - 1;
    end
    
    vision_data_size = first_layer;
    
    max_target_code = max(target_codes);
    
    % prepocet uhla natocenia zraku na radiany
    ray_radians = ray_degree*pi/180;
   
    % zabalenie nastavenii organizmu a predatora
    org_settings = {
        target_codes(1), organism_size, organism_step_size, organism_step_size_range
    };             

    goal_size = floor(goal_mode_goal_size/2);

    % tvorba konfiguracneho suboru
    CONF = {
       'gener_n' gener_n; 
       'pop_n' pop_n;
       'sim_time' sim_time; 
       'ray_degree' ray_degree;
       'ray_step_size' ray_step_size; 
       'vision_mode' vision_mode;
       'base_map_size' base_map_size;
       'max_sight' max_sight;
       'organism_line_color' organism_line_color;
       'organism_line_style' organism_line_style;
       'organism_marker' organism_marker;
       'organism_marker_color' organism_marker_color;
       'barrier_marker' barrier_marker;
       'barrier_marker_color' barrier_marker_color;
       'collision_marker_color' collision_marker_color;
       'collision_marker' collision_marker;
       'food_marker' food_marker;
       'food_marker_color' food_marker_color;
       'starting_position_marker_color' starting_position_marker_color;
       'starting_position_marker' starting_position_marker;
       'aniate_refresh_period' aniate_refresh_period;
       'border_width' border_width;
       'experiment_name' experiment_name;
       'save_dir' save_dir;
       'save_animations_dir' save_animations_dir;
       'save_period' save_period;
       'target_codes' target_codes;
       'output_range' output_range;
       'animate_period' animate_period;
       'animate_display_starting_position' animate_display_starting_position;
       'animate_only_better' animate_only_better;
       'animate_legend' animate_legend;
       'save_animations' save_animations;
       'save_old_experiment_name_with_new_settings' save_old_experiment_name_with_new_settings;
       'max_target_code' max_target_code;
       'vision_data_size' vision_data_size;
       'ray_radians' ray_radians;
       'random_start_position' random_start_position;
       'animate_movement_lines' animate_movement_lines;
       'save_only_better' save_only_better;
       'islands_n' islands_n;
       'goal_size' goal_size;
       'genes_n' genes_n;
    }; 
    
    % pridanie dodatocnych parametrov
    CONF = CONF';
    CONF = struct(CONF{:});
    CONF.experiment_environments = experiment_environments;
    CONF.org_settings = org_settings;
end