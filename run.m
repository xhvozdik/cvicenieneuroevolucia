clc; clear ;
addpath(genpath('_sources'));
addpath(genpath('_todo'));

% konfiguracia - volba konfiguracneho suboru
configuration = config;

% pokust sa nacitat ulozeny stav?
try_load_state = 1;
% nova konfiguracia experimentu (v pripade nacitavania) ak je aktivne, 
% pri nacitani sa vyuziju nastavenia configuration definovane vyssie,
% inak nastavenia pritomne v ulozenej pozicii
new_settings = 1;
% animovat najlepsieho jedinca nacitanej populacie
animate_best_org = 1;

if try_load_state
	% pokusi sa nacitat simulaciu (zobrazi dialogove okno) v pripade ze sa stav nenacita 
	% (naprikad uzavretie okna pouzivatelom) zapocen novu simulaciu podla aktualneho stavu 
	% konfiguracneho suboru 
	load_experiment(configuration, new_settings, animate_best_org);
else
	% inicializuje aplikaciu s aktualnym stavom konfiguracneho suboru
	Training(config);
end
