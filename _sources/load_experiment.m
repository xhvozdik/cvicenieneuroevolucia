function [] = load_experiment(NEW_C, new_settings, animate)
%LOAD_EXPERIMENT Summary of this function goes here
%   Detailed explanation goes here

    % pokus o ziskanie suboru pomocou file dialogu
    [experiment_name,  path] = uigetfile('*.mat');
    if isequal(experiment_name,0)
        % ak neuspesne, inicializuje sa s novymi nastaveniami
        disp('No file selected, starting new training');
        Training(NEW_C);
    else 
        % nacitanie dat zo suboru
        settings = 'old';
        file_path = fullfile(path, experiment_name);
        load(file_path)                                            %#ok<LOAD>

        C = fix_conf_compatibility(C);                             %#ok<*NODEF>

        % pripadna aplikacia novych nastaveni
        if new_settings
            old_experiment_name = C.experiment_name; 
            old_islands_n = C.islands_n;
            C = NEW_C;
            C.islands_n = old_islands_n;
            if C.save_old_experiment_name_with_new_settings
                C.experiment_name = old_experiment_name;
            end
            settings = 'new';
        end
        
        % animacia posledneho priebehu
        % najlesi jedinec sa predpoklada na prvej pozicii v populacii
        if animate
            if best_isle > 0
                chromozome = Pop{best_isle}(1, :);                       
            else
                chromozome = Pop(1, :); 
            end
            V = Visualizator(C);
            animate_chromozome(V, C, chromozome, global_gen);
        end        
        
        fprintf('File %s loaded with %s settings\n\n', experiment_name, settings);
        Training(C, global_gen, Pop, PLOT_DATA);
    end
end

% funkcia urcena k oprave pripadnych odlisnosti v konfiguracnych suboroch ako absencia noveho nastavenia
function conf = fix_conf_compatibility(conf)
end

