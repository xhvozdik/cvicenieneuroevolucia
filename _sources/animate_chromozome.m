function [] = animate_chromozome(V, C, chromozome, gen)
    %SIMULATE_FOR_ANIMATION simulacia najlepsieho chromozomu PGA na ziskanie dat pre animaciu
    ANIMATION_DATA = zeros(numel(C.experiment_environments), 3, C.sim_time + 1);

    V.current_gen = gen;
    
    % pre kazde prostredie
    i = 0;
    for map = C.experiment_environments
        i = i + 1;
        % beh simulacie pre potreby animacie
        % SIM_DATA = {org_x, org_y, collisions, goal_coords}
        ANIMATION_DATA(i, :, :) = Simulation(C, map, chromozome, 0);
    end
    
    V.animate_all_general(ANIMATION_DATA);
end