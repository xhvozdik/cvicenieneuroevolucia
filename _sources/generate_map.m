function [map, id_map, goal_coords, starting_positions, w, h] = generate_map(type, base_map_size, target_codes)
    if type == 1 
    %	+--------------+
    %	|              |
    %	|              |
    %	|      S       |
    %	|              |
    %	|          X   |
    %	+--------------+    
    
        w = base_map_size;
        h = base_map_size;
        goal_coords = [round(0.8*w), round(0.2*h)];
        starting_positions = [round(0.5*w), round(0.5*h)];
        
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        
    elseif type == 2
    %	+-----------------------------+
    %	|                             |
    %	|    X                        |
    %	|                      S      |
    %	|                             |
    %	|                             |
    %	+-----------------------------+
    
        w = base_map_size;
        h = floor(base_map_size/2);
        goal_coords = [round(0.165*w), round(0.8*h)];
        starting_positions = [round(0.9*w), round(0.5*h)];
        
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        
    elseif type == 3
    %	+-----------------+
    %	|      |-----|    |
    %	|  S   |-----|  X |
    %	|      +-----+    |
    %	|                 |
    %	|          +------+
    %	+----+     |------|
    %	|----|     |------|
    %	+-----------------+
    
        w = base_map_size;
        h = base_map_size;
        goal_coords = [round(0.9*w), round(0.9*h)];
        starting_positions = [round(0.125*w), round(0.9*h)];
        
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(1:floor(h*0.2), 1:floor(w*0.25)) = target_codes(2);
        map(1:floor(h*0.3), floor(w*0.65):end) = target_codes(2);
        map(floor(h*0.55):end, floor(w*0.35):floor(w*0.7)) = target_codes(2);
        
    elseif type == 4
    %	+------------------------+
    %	|   |-|           |-|    |
    %	| S |-|           |-| X  |
    %	|   |-|    +-+    |-|    |
    %	|   +-+    |-|    +-+    |
    %	|          |-|           |
    %	|          |-|           |
    %	+------------------------+
    
        w = floor(base_map_size*1.1);
        h = floor(base_map_size*0.7);
        goal_coords = [round(0.9*w), round(0.9*h)];
        starting_positions = [round(0.12*w), round(0.86*h)];
        
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(floor(h*0.4):end, floor(w*0.70):floor(w*0.8)) = target_codes(2);
        map(1:floor(h*0.6), floor(w*0.45):floor(w*0.55)) = target_codes(2);
        map(floor(h*0.4):end, floor(w*0.2):floor(w*0.3)) = target_codes(2);
        
    elseif type == 5
    %	+-------------------------+
    %	|   X         |--|        |
    %	|             |--|        |
    %	+-------+     +--+        |
    %	+-------+                 |
    %	|                         |
    %	|           +-------------+
    %	|           +-------------+
    %	|                         |
    %	+-----------------+       |
    %	+-----------------+       |
    %	|   S                     |
    %	+-------------------------+
    
        w = base_map_size;
        h = floor(base_map_size*1.1);
        goal_coords = [round(0.1*w), round(0.9*h)];
        starting_positions = [round(0.1*w), round(0.1*h)];
        
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(floor(h*0.2):floor(h*0.3), 1:floor(w*0.8)) = target_codes(2);
        map(floor(h*0.5):floor(h*0.6), floor(w*0.5):end) = target_codes(2);
        map(floor(h*0.7):floor(h*0.8), 1:floor(w*0.33)) = target_codes(2);
        map(floor(h*0.8):end, floor(w*0.6):floor(w*0.75)) = target_codes(2);
        
    elseif type == 6
    %	+---------------------------------+
    %	|                                 |
    %	|                                 |
    %	|                                 |
    %	+----+  +----+                    |
    %	|            |                    |
    %	|         S  |  X                 |
    %	|            |                    |
    %	+---------------------------------+
        h = base_map_size;
        w = h*1.2;
        starting_positions = [floor(w*0.35), floor(h*0.25)];
        goal_coords = [floor(w*0.5), floor(h*0.25)];
        
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(floor(h*0.5):floor(h*0.525), 1:floor(w*0.195)) = target_codes(2);
        map(floor(h*0.5):floor(h*0.525), floor(w*0.2250):floor(w*0.4375)) = target_codes(2);
        map(1:floor(h*0.525), floor(w*0.42):floor(w*0.4375)) = target_codes(2);
        
    elseif type == 7
    %	+-----------------------------------+
    %	|                                   |
    %	|                               X   |
    %	+--------------------+              |
    %	|                    |              |
    %	|                    |              |
    %	+-----+  +-----+     |              |
    %	|              |     +              |
    %	|              |                    |
    %	|           S  |     +              |
    %	|              |     |              |
    %	+-----------------------------------+
        
        w = base_map_size*1.2;
        h = w;
        starting_positions = [floor(w*0.35), floor(h*0.25)];
        goal_coords = [floor(h*0.92), floor(w*0.92)];

        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(floor(h*0.42):floor(h*0.44), 1:floor(w*0.195)) = target_codes(2);
        map(floor(h*0.42):floor(h*0.44), floor(w*0.225):floor(w*0.44)) = target_codes(2);
        map(1:floor(h*0.44), floor(w*0.42):floor(w*0.44)) = target_codes(2);

        map(floor(h*0.625):floor(h*0.648), 1:floor(w*0.648)) = target_codes(2);
        map(1:floor(h*0.226), floor(w*0.625):floor(w*0.648)) = target_codes(2);
        map(floor(h*0.26):floor(h*0.648), floor(w*0.625):floor(w*0.648)) = target_codes(2);
        
    elseif type == 8
    %	+-----------------------------------+
    %	|                                   |
    %	+-------+  +---------------+    X   |
    %	|                          |        |
    %	+--------------------+     |        |
    %	|                    |     |        |
    %	+-----+  +-----+     +     |        |
    %	|              |           |        |
    %	|           S  |     +     |        |
    %	|              |     |     |        |
    %	+--------------+-----+-----+--------+
        
        w = base_map_size*1.2;
        h = w;
        starting_positions = [floor(w*0.35), floor(h*0.25)];
        goal_coords = [floor(h*0.92), floor(w*0.92)];

        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(floor(h*0.42):floor(h*0.44), 1:floor(w*0.195)) = target_codes(2);
        map(floor(h*0.42):floor(h*0.44), floor(w*0.225):floor(w*0.44)) = target_codes(2);
        map(1:floor(h*0.44), floor(w*0.42):floor(w*0.44)) = target_codes(2);

        map(floor(h*0.625):floor(h*0.648), 1:floor(w*0.648)) = target_codes(2);
        map(1:floor(h*0.226), floor(w*0.625):floor(w*0.648)) = target_codes(2);
        map(floor(h*0.26):floor(h*0.648), floor(w*0.625):floor(w*0.648)) = target_codes(2);

        map(floor(h*0.833):floor(h*0.855), 1:floor(w*0.233)) = target_codes(2);
        map(floor(h*0.833):floor(h*0.855), floor(w*0.267):floor(w*0.855)) = target_codes(2);
        map(1:floor(h*0.855), floor(w*0.833):floor(w*0.855)) = target_codes(2);
        
    elseif type == 9
    %	+---------------+----------------+
    %	|               |                |
    %	|               |                |
    %	|               |                |
    %	|               +                |
    %	|          X         S           |
    %	|               +                |
    %	|               |                |
    %	|               |                |
    %	|               |                |
    %	+--------------------------------+
    
        h = base_map_size;
        w = h*1.2;

        starting_positions = [floor(w*0.6), floor(h*0.5)];
        goal_coords = [floor(w*0.4), floor(h*0.5)];
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(1:floor(h*0.49), floor(w*0.49):floor(w*0.51)) = target_codes(2);
        map(floor(h*0.51):end, floor(w*0.49):floor(w*0.51)) = target_codes(2);
        
    elseif type == 10
    %	+---------------+----------------+
    %	|               |                |
    %	|               |                |
    %	|               |                |
    %	|               +                |
    %	|          S         X           |
    %	|               +                |
    %	|               |                |
    %	|               |                |
    %	|               |                |
    %	+--------------------------------+
    
        h = base_map_size;
        w = h*1.2;

        starting_positions = [floor(w*0.4), floor(h*0.5)];
        goal_coords = [floor(w*0.6), floor(h*0.5)];
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(1:floor(h*0.49), floor(w*0.49):floor(w*0.51)) = target_codes(2);
        map(floor(h*0.51):end, floor(w*0.49):floor(w*0.51)) = target_codes(2);
        
    elseif type == 11
    %	+-------------------------+
    %	|                         |
    %	|    +---------------+    |
    %	|    |               |    |
    %	|    |    +-----+    |    |
    %	|    |    +  X  |    |    |
    %	|    |          |    |    |
    %	|    +----------+    |    |
    %	|                    |    |
    %	+--------------------+    |
    %	|  s                      |
    %	+-------------------------+
    
        h = base_map_size;
        w = h;
        
        starting_positions = [floor(h*0.1), floor(w*0.1)];
        goal_coords = [floor(h*0.5), floor(w*0.5)];
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(floor(h*0.19):floor(h*0.21), 1:floor(w*0.81)) = target_codes(2);
        map(floor(h*0.39):floor(h*0.41), floor(w*0.19):floor(w*0.61)) = target_codes(2);
        map(floor(h*0.59):floor(h*0.61), floor(w*0.39):floor(w*0.61)) = target_codes(2);
        map(floor(h*0.79):floor(h*0.81), floor(w*0.19):floor(w*0.81)) = target_codes(2);
        
        map(floor(h*0.39):floor(h*0.81), floor(w*0.19):floor(w*0.21)) = target_codes(2);
        map(floor(h*0.49):floor(h*0.61), floor(w*0.39):floor(w*0.41)) = target_codes(2);
        map(floor(h*0.39):floor(h*0.61), floor(w*0.59):floor(w*0.61)) = target_codes(2);
        map(floor(h*0.19):floor(h*0.81), floor(w*0.79):floor(w*0.81)) = target_codes(2);
        
    elseif type == 12
    %	+-------------------------+
    %	|  S                      |
    %	+--------------------+    |
    %	|                    |    |
    %	|    +----------+    |    |
    %	|    |          |    |    |
    %	|    |    +  X  |    |    |
    %	|    |    +-----+    |    |
    %	|    |               |    |
    %	|    +---------------+    |
    %	|                         |
    %	+-------------------------+
    
        h = base_map_size;
        w = h;
        
        starting_positions = [floor(h*0.1), floor(w*0.9)];
        goal_coords = [floor(h*0.5), floor(w*0.5)];
        [map, id_map] = generate_empty_arena(w, h, target_codes);
        map(floor(h*0.79):floor(h*0.81), 1:floor(w*0.81)) = target_codes(2);
        map(floor(h*0.59):floor(h*0.61), floor(w*0.19):floor(w*0.61)) = target_codes(2);
        map(floor(h*0.39):floor(h*0.41), floor(w*0.39):floor(w*0.61)) = target_codes(2);
        map(floor(h*0.19):floor(h*0.21), floor(w*0.19):floor(w*0.81)) = target_codes(2);
        
        map(floor(h*0.19):floor(h*0.61), floor(w*0.19):floor(w*0.21)) = target_codes(2);
        map(floor(h*0.39):floor(h*0.51), floor(w*0.39):floor(w*0.41)) = target_codes(2);
        map(floor(h*0.39):floor(h*0.61), floor(w*0.59):floor(w*0.61)) = target_codes(2);
        map(floor(h*0.19):floor(h*0.81), floor(w*0.79):floor(w*0.81)) = target_codes(2);
        
    elseif type == 13
    %   +---------------------------+
    %   |                           |
    %   |   +-------------------+   |
    %   |   |                   |   |
    %   |   |   +-----------+   |   |
    %   |   |   |           |   |   |
    %   |   |   |   ++ ++   |   |   |
    %   |   |   +   |   |   |   +   |
    %   | S |       | X |   |       |
    %   |   |   +   |   |   |   +   |
    %   +---+---+---+---+---+---+---+
    
        h = base_map_size;
        w = h;
        
        starting_positions = [floor(w*0.1), floor(h*0.1)];
        goal_coords = [floor(w*0.6), floor(h*0.1)];
        [map, id_map] = generate_empty_arena(w*1.2, h, target_codes);
        
        map(1:floor(h*0.81), floor(w*0.19):floor(w*0.21)) = target_codes(2);
        map(1:floor(h*0.19), floor(w*0.39):floor(w*0.41)) = target_codes(2);
        map(floor(h*0.22):floor(h*0.61), floor(w*0.39):floor(w*0.41)) = target_codes(2);
        map(1:floor(h*0.41), floor(w*0.49):floor(w*0.51)) = target_codes(2);
        map(1:floor(h*0.41), floor(w*0.69):floor(w*0.71)) = target_codes(2);
        map(1:floor(h*0.61), floor(w*0.79):floor(w*0.81)) = target_codes(2);
        map(1:floor(h*0.38), floor(w*0.99):floor(w*1.01)) = target_codes(2);
        map(floor(h*0.42):floor(h*0.81), floor(w*0.99):floor(w*1.01)) = target_codes(2);
        
        map(floor(h*0.39):floor(h*0.41), floor(w*0.49):floor(w*0.58)) = target_codes(2);
        map(floor(h*0.39):floor(h*0.41), floor(w*0.62):floor(w*0.71)) = target_codes(2);
        map(floor(h*0.59):floor(h*0.61), floor(w*0.39):floor(w*0.81)) = target_codes(2);
        map(floor(h*0.79):floor(h*0.81), floor(w*0.19):floor(w*1.01)) = target_codes(2);
        
        w = w * 1.2;
        
    elseif type == 14
    %	+-------------------------------+
    %	|               |               |
    %	|     +----+    |    +----+     |
    %	|     |----|    |    |----|     |
    %	|     |----|    +    |----|     |
    %	| S   |----|         |----|   X |
    %	|     |----|    +    |----|     |
    %	|     |----|    |    |----|     |
    %	|     +----+    |    +----+     |
    %	|               |               |
    %	+-------------------------------+
    
        h = base_map_size;
        w = h;
        
        starting_positions = [floor(w*0.1), floor(h*0.5)];
        goal_coords = [floor(w*1.1), floor(h*0.5)];
        [map, id_map] = generate_empty_arena(w*1.2, h, target_codes);
        
        map(floor(h*0.3):floor(h*0.7), floor(w*0.2):floor(w*0.4)) = target_codes(2);
        map(1:floor(h*0.48), floor(w*0.58):floor(w*0.62)) = target_codes(2);
        map(floor(h*0.52):end, floor(w*0.58):floor(w*0.62)) = target_codes(2);
        map(floor(h*0.3):floor(h*0.7), floor(w*0.8):floor(w*1)) = target_codes(2);
        
        w = w * 1.2;
        
    elseif type == 15
    %	+-------------------------------+
    %	|               |               |
    %	|     +----+    |    +----+     |
    %	|     |----|    |    |----|     |
    %	|     |----|    +    |----|     |
    %	| X   |----|         |----|   S |
    %	|     |----|    +    |----|     |
    %	|     |----|    |    |----|     |
    %	|     +----+    |    +----+     |
    %	|               |               |
    %	+---------------+---------------+
        
        h = base_map_size;
        w = h;
        
        starting_positions = [floor(w*1.1), floor(h*0.5)];
        goal_coords = [floor(w*0.1), floor(h*0.5)];
        [map, id_map] = generate_empty_arena(w*1.2, h, target_codes);
        
        map(floor(h*0.3):floor(h*0.7), floor(w*0.2):floor(w*0.4)) = target_codes(2);
        map(1:floor(h*0.48), floor(w*0.58):floor(w*0.62)) = target_codes(2);
        map(floor(h*0.52):end, floor(w*0.58):floor(w*0.62)) = target_codes(2);
        map(floor(h*0.3):floor(h*0.7), floor(w*0.8):floor(w*1)) = target_codes(2);
        
        w = w * 1.2;
    end
end

% vytvorenie prazdneho prostredia s ohranicenim
function [map, id_map] = generate_empty_arena(w, h, target_codes)
    map = ones(h, w)*target_codes(2);
    map(2:h-1, 2:w-1) = target_codes(1);
    id_map = zeros(h, w);
end