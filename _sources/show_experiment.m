function [] = show_experiment()
%SHOW_EXPERIMENT Summary of this function goes here
%   Detailed explanation goes here

    % pokus o ziskanie suboru pomocou file dialogu
    [experiment_name,  path] = uigetfile('*.mat');
    if isequal(experiment_name,0)
        % ak neuspesne, inicializuje sa s novymi nastaveniami
        disp('No file selected');
    else 
        % nacitanie dat zo suboru
        file_path = fullfile(path, experiment_name);
        load(file_path)                                            %#ok<LOAD>

        C = fix_conf_compatibility(C);                             %#ok<*NODEF>

        % animacia posledneho priebehu
        if best_isle > 0
            chromozome = Pop{best_isle}(1, :);                       
        else
            chromozome = Pop(1, :); 
        end
        V = Visualizator(C);
        animate_chromozome(V, C, chromozome, global_gen);
    end
end

% funkcia urcena k oprave pripadnych odlisnosti v konfiguracnych suboroch ako absencia noveho nastavenia
function conf = fix_conf_compatibility(conf)
end