function [S, S_ENV, S_DATA] = init_simulation_structs(C, map_id, all_org_n, training)
    sim_time = C.sim_time;
    
    % simulacne premenne
    S = {
        'sim_time' sim_time;
        'training' training;
        'org_n' all_org_n;
        'visual_offset' C.vision_data_size/2;
        'absolute_ray' floor(C.vision_data_size/4);
        'ray_radians' C.ray_radians;
        'ray_step_size' C.ray_step_size;
        'vision_mode' C.vision_mode;
        'max_sight' C.max_sight;
        'max_target_code' C.max_target_code;
        'output_range' C.output_range;
        'step_size' C.org_settings{3};
        'step_size_range' C.org_settings{4};
        'node_size' C.org_settings{2};
        'random_start_position' C.random_start_position;
        'target_codes' C.target_codes;
        'empty_code' C.target_codes(1);
        'map_id' map_id;
        'max_env_distance' 0;
        
        't' 0;
        'org_i' 0;
        'prev_i' 0;
        'phi' 0;
        'mov_' 0;
        'org_indexes' 1:all_org_n;
        'visual_output' zeros(C.vision_data_size, 1);
        'org_lifetime' zeros(all_org_n, 1);
    };
    S = S';
    S = struct(S{:});

    % inicializaca prostredia
    [map, id_map, goal_coords, starting_positions, w, h] = generate_map(map_id, C.base_map_size, C.target_codes);
    max_distance = sqrt(w^2 + h^2);
    [empty_rows, empty_cols] = find(map == C.target_codes(1));
    e_cols_size = size(empty_cols, 1);
    e_rows_size = size(empty_rows, 1);
    
    % pridanie ciela do mapy
    gs = C.goal_size;
    map(goal_coords(2) - gs:goal_coords(2) + gs, ...
        goal_coords(1) - gs:goal_coords(1) + gs) = C.target_codes(3);

    S.max_env_distance = max_distance;
    
    % premenne prostredia
    S_ENV = {    
        'map' map;
        'id_map' id_map;
        'w' w;
        'h' h;
        'empty_cols' empty_cols;
        'empty_rows' empty_rows;
        'empty_cols_size' e_cols_size;
        'empty_rows_size' e_rows_size;
        'map_starting_positnions' starting_positions;
        'goal_coords' goal_coords;
        'goal_coords_size' size(goal_coords, 1);
        'target_codes' C.target_codes;
        'goal_size' C.goal_size;
    };
    S_ENV = S_ENV';
    S_ENV = struct(S_ENV{:});
      
    % polia pre zber vysledkov
    S_DATA = {
        'collisions' ones(all_org_n, sim_time + 1)*C.target_codes(1);
        'org_x' inf(all_org_n, sim_time + 1);
        'org_y' inf(all_org_n, sim_time + 1);
    };
    S_DATA = S_DATA';
    S_DATA = struct(S_DATA{:});
end