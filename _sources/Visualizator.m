classdef Visualizator < handle & matlab.mixin.Copyable
    % VISUALISATOR Vizualizator - trieda schopna animovat priebeh simulacie umeleho zivota a vykreslovat grafy priebehu trenovania
    properties
        C;
        
        animation_initialized = 0;
        animation_figure;
        animation_axes;
        
        global_gen = 0;
        current_gen = 0;
    end
    
    methods
        function v = Visualizator(C)
            v.C = C;
        end
        
        function animate_movement(v, type, sim_time, x, y, colls)
            runtime = 0;
            [map, ~, goal_coords, ~, w, h] = generate_map(type, v.C.base_map_size, v.C.target_codes);
            
            try 
                axes(v.animation_axes);
                cla(v.animation_axes);
            catch
                v.animation_figure = figure('Name', 'Animation of simulation');
                v.animation_axes = axes;
                v.animation_initialized = 1;
            end
            
            map_c = animatedline('LineStyle', 'none', 'Marker', v.C.barrier_marker, 'MarkerFaceColor', v.C.barrier_marker_color, 'MarkerEdgeColor', v.C.barrier_marker_color);  
            
            if v.C.animate_display_starting_position
                starting_c = animatedline('LineStyle', 'none', 'Marker', v.C.starting_position_marker, 'MarkerFaceColor', v.C.starting_position_marker_color, 'MarkerEdgeColor', v.C.starting_position_marker_color);
                addpoints(starting_c, x(1), y(1));
            end
            
            org_line = animatedline('Color', v.C.organism_line_color, 'LineStyle', v.C.organism_line_style);
            org_curve = animatedline('LineStyle', 'none', 'Marker', v.C.organism_marker, 'MarkerFaceColor', v.C.organism_marker_color, 'MarkerEdgeColor', v.C.organism_marker_color);
            org_coll_c = animatedline('LineStyle', 'none', 'Marker', v.C.collision_marker, 'MarkerFaceColor', v.C.collision_marker_color, 'MarkerEdgeColor', v.C.collision_marker_color);

            org_end = find(x == inf, 1) - 1;
            if isempty(org_end); org_end = sim_time; end
            runtime = max(runtime, org_end);

            if v.C.animate_display_starting_position
                addpoints(starting_c, x(1), y(1));
            end

            set(v.animation_axes, 'XLim', [0 w], 'YLim', [0, h]);
            grid on;
            
            food_c = animatedline('LineStyle', 'none', 'Marker', v.C.food_marker, 'MarkerFaceColor', v.C.food_marker_color, 'MarkerEdgeColor', v.C.food_marker_color);
            for i = 1:size(goal_coords, 1)
                map(goal_coords(i, 2)-1:goal_coords(i, 2)+1, goal_coords(i, 1)-1:goal_coords(i, 1)+1) = v.C.target_codes(3);
            end

            [cols, rows] = find(map == v.C.target_codes(2));
            addpoints(map_c, rows, cols);
          
            [cols, rows] = find(map == v.C.target_codes(3)); 
            addpoints(food_c, rows, cols);
            
            if v.C.animate_legend
                v.animate_legend();
            end
            
            period = v.C.aniate_refresh_period;
            last_round = 0;
            
            for t = 1:period:runtime+period    
                if ~ishandle(v.animation_figure) || last_round
                    break
                end
            
                if t > runtime
                    last_round = 1;
                    t = runtime;                                            %#ok<FXSET>
                end
                
                clearpoints(org_curve);
                if v.C.animate_movement_lines
                    addpoints(org_line, x(t), y(t));
                end
                addpoints(org_curve, x(t), y(t)); 

                if colls(t) == v.C.target_codes(2)
                    addpoints(org_coll_c, x(t), y(t));
                end                         
                drawnow 
            end
            
            if ishandle(v.animation_figure) && v.C.save_animations
                path = fullfile(pwd, v.C.save_animations_dir, v.C.experiment_name);
                if ~exist(path, 'dir')
                    mkdir(path)
                end
                full_path = [path, '\', v.C.experiment_name, '(', int2str(v.current_gen), ...
                            ',', int2str(v.current_gen + v.global_gen) , ')_map', ...
                            int2str(type),'_', datestr(now,'dd-mm-yy_HH-MM-SS')];
                savefig(v.animation_figure, full_path);
            end
        end
        
        function animate_all_general(v, ANIMATION_DATA)
            c = 0;
            for map = v.C.experiment_environments
                c = c + 1;
                v.animate_movement(map, ...
                                   v.C.sim_time, ...
                                   squeeze(ANIMATION_DATA(c, 1, :)), ...
                                   squeeze(ANIMATION_DATA(c, 2, :)), ...
                                   squeeze(ANIMATION_DATA(c, 3, :)) ...
                                   );
            end
        end
        
        function animate_legend(v)
            if v.C.animate_display_starting_position == 1
                lgd = legend(v.animation_axes, 'Barrier', 'Start', 'Organism movement', 'Organism', 'Collisions',  'Goal', 'Location', 'northoutside', 'Orientation','horizontal');
                lgd.NumColumns = 3;
            else
                lgd = legend(v.animation_axes, 'Barrier', 'Organism movement', 'Organism', 'Collisions',  'Goal', 'Location', 'northoutside', 'Orientation','horizontal');
                lgd.NumColumns = 3;
            end
        end
    end
end