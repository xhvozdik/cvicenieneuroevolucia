function [] = save_experiment(C, global_gen, Pop, PLOT_DATA, best_isle)
%SAVE_EXPERIMENT Ulozenie dat experimentu na zvolenu cestu

    if ~exist('best_isle', 'var'); best_isle = 0; end
    if ~exist('PLOT_DATA', 'var'); PLOT_DATA = {}; end

    path = fullfile(pwd, C.save_dir, C.experiment_name);                                                                                                                
    
    % ulozenie dat do suboru v nastavenej ceste
    if ~exist(path, 'dir')
        mkdir(path)
    end
    full_path = [path, '/', C.experiment_name, '(', int2str(global_gen) , ')_', datestr(now,'dd-mm-yy_HH-MM-SS')];
    save(full_path, 'C', 'global_gen', 'Pop', 'best_isle', 'PLOT_DATA');
end