function [SIM_DATA] = Simulation(C, map_id, chromozome, train_sim)
    %SIMULATION Simulacia pohybu organizmu v prostredi

    org_n = size(chromozome, 1);
    
    % incicializacia 
    [S, S_ENV, S_DATA] = init_simulation_structs(C, map_id, org_n, train_sim);
    
    % inicializacia neuronovej siete
    N = cell(org_n, 1);
    for i = 1:(size(chromozome, 1))
        N{i} = init_network(C, chromozome(i, :));
    end
    
    for i = S.org_indexes
        [x, y] = get_starting_position(S, S_ENV, S.node_size, i);
        S_DATA.org_x(i, 1) = x; 
        S_DATA.org_y(i, 1) = y;
    end
    
    SIM_DATA = run(S, S_ENV, S_DATA, N);
end

% beh simulacie
function SIM_DATA = run(S, S_ENV, S_DATA, N)
    phis = zeros(S.org_n, 1);
    
    % priebeh simulacie
    for t = 2:S.sim_time + 1
        S.t = t;
        
        % krok simulacie pre jeden organizmus
        for i = S.org_indexes
            S.org_i = i;
            S.phi = phis(i);
            
            % zber dat z vizualneho systemu
            visual_data = sim_sensory_system(S, S_ENV, S_DATA.org_x(i, t-1), S_DATA.org_y(i, t-1));
            
            % vyhodnotenie smeru pohybu na zaklade 
            [phi] = sim_brain(S, N{i}, visual_data);
            S.phi = S.phi + phi;
            
            % pohyb organizmu zvolenym smerom
            [x, y, c] = sim_kinetic_system(S, S_ENV, S_DATA.org_x(i, t-1), S_DATA.org_y(i, t-1));
            
            % ukladanie ziskanych dat
            S_DATA.org_x(i, t) = x;
            S_DATA.org_y(i, t) = y;
            S_DATA.collisions(i, t) = c;
            
            phis(i) = S.phi;
            
            % spracovanie kolizie organizmu s cielom
            if c == S.target_codes(3)
                [S] = clear_org(S, i);
                continue
            end
        end
        
        % a nezije ziadny organizms, simulacia konci
        if numel(S.org_indexes) == 0
            break
        end
    end
    
    % ukonci zivot zvysnych organizmov
    for i = S.org_indexes
        [S] = clear_org(S, i);
    end

    % zber simulacnych dat v pripade pre potreby vyhodnocovania 
    if S.training
        SIM_DATA = {
            S_DATA.org_x, S_DATA.org_y, S_DATA.collisions, ...
            S.org_lifetime, S_ENV.goal_coords
        };
    % pre potreby animacie
    else
        SIM_DATA = [S_DATA.org_x; S_DATA.org_y; S_DATA.collisions];
    end
end

% ukonci zivot organizmu
function [S] = clear_org(S, i)
    S.org_indexes(S.org_indexes == i) = [];
    S.org_lifetime(i) = S.t;
end

% simulacia zmyslovej sustavy
function visual_data = sim_sensory_system(S, S_ENV, x, y)
    if S.vision_mode == 1
        visual_data = sensory_input_rays(S, S_ENV, x, y);
    else
        visual_data = sensory_input_vicinity(S, S_ENV, x, y);
    end
end   

% simulacia zraku za pomoci lucov
function visual_data = sensory_input_rays(S, S_ENV, x, y)
    phi = S.phi;
    empty = S.empty_code;      
    ray_radians_ = S.ray_radians;
    ray_step_size_ = S.ray_step_size;    
    offset = S.visual_offset;
    abs_ray = S.absolute_ray;
    
    w_ = S_ENV.w; h_ = S_ENV.h; map_ = S_ENV.map;
    x_ = round(x); y_ = round(y);

    % vysielanie lucov prostredim
    visual_data = S.visual_output;
    for ray = -abs_ray:abs_ray
        ray_x = x_; ray_y = y_; ray_step = 0;
        sigma = phi + ray * ray_radians_;
        delta_ray_x = cos(sigma)*ray_step_size_;
        delta_ray_y = sin(sigma)*ray_step_size_;

        collision = empty;
        while (collision == empty)
            ray_step = ray_step + ray_step_size_;
            ray_x = ray_x + delta_ray_x;
            ray_y = ray_y + delta_ray_y;
            collision = map_(round(min(h_, max(1, ray_y))), round(min(w_, max(1, ray_x))));
        end
        visual_data(ray + abs_ray + 1) = ray_step;
        visual_data(ray + abs_ray + offset + 1) = collision;
    end
end

% simulacia zraku okolia organizmu
function visual_data = sensory_input_vicinity(S, S_ENV, x, y)
    x_ = round(x); y_ = round(y);

    count = 1;
    w_ = S_ENV.w; h_ = S_ENV.h; map_ = S_ENV.map;

    visual_data = S.visual_output;
    for x_i = x_ - S.max_sight : x_ + S.max_sight
        for y_i =  y_ - S.max_sight : y_ + S.max_sight
            if x_i == x_ && y_i == y_
                continue;
            elseif x_i <= 0 || y_i <= 0 || x_i > w_ || y_i > h_
                visual_data(count) = 1;
            else
                visual_data(count) = map_(y_i, x_i);
            end
            count = count + 1;
        end
    end
end

% simulacia pohyovej sutavy
function [x_t, y_t, coll] = sim_kinetic_system(S, S_ENV, x_, y_)
    size = S.node_size;
    codes = S.target_codes;

    x_t = cos(S.phi)*S.step_size + x_;
    y_t = sin(S.phi)*S.step_size + y_;

    x = round(min(S_ENV.w-size+1, max(1, x_t)));
    y = round(min(S_ENV.h-size+1, max(1, y_t)));
    coll = S_ENV.map(y:y+size-1, x:x+size-1);

    if any(coll(:) == codes(2))
        coll = codes(2);
        x_t = x_; 
        y_t = y_;
    else
        if any(coll(:) == codes(3))
            coll = codes(3);
        else
            coll = codes(1);
        end
    end
end

% ziskanie startovacej pozicie prostredia, ak je dynamicka vygeneruje sa 
function [x, y] = get_starting_position(S, S_ENV, node_size, index)
    if S.random_start_position
       [x, y] = generate_valid_position(S_ENV, node_size, S.empty_code); 
    else
       sp = S_ENV.map_starting_positnions;
       sp = sp(mod(index - 1, size(sp, 1)) +  1, :);
       x = sp(1);
       y = sp(2);
    end
end

% vygeneruje validnu poziciu na volnom priestranstve
function [x, y] = generate_valid_position(S_ENV, size, empty_code)
    while 1
        x = S_ENV.empty_cols(randi([1, S_ENV.empty_cols_size]));
        y = S_ENV.empty_rows(randi([1, S_ENV.empty_rows_size]));
        try
            if S_ENV.map(y:y+size, x:x+size) == empty_code
                break
            end
        catch
        end
    end
end